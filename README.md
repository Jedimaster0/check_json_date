Dependancies
===============
* Data::Dumper
* DateTime
* DateTime::Format::Strptime
* File::Basename
* Hash::Flatten
* JSON::Parse
* LWP::UserAgent
* Monitoring::Plugin

NOTE: Use CPAN or cpanminus for anything that your normal repos don't have. You may have to format the module name differently to install with dnf,yum,apt-get,etc....

Example: `sudo dnf install perl-DateTime-Format-Strptime`


Installation
===============
* If the perl package doesn't work, use pp on the .pl to create one for your system.
    * It's a giant pain resolving dependancies, but use your package manager first, then cpan when installing perl modules. cpan doesn't keep them up to date automatically.
    * `pp -o check_json_date check_json_date.pl`
* Copy the check_json_date file to your nagios plugin directory.
    * `cp check_json_date /usr/local/nagios/libexec`
* Ensure execute permissions are set on user/group.
    * `chmod ug+x /usr/local/nagios/libexec/check_json_date`
    * `chown nagios:nagios /usr/local/nagios/libexec/check_json_date`


Usage
==============

    Retrieve JSON data from an http/s url and check an object's date attribute to determine if the data is stale.

    --help      shows this message
    --version   shows version information

    USAGE: ./check_json_date -U http://www.convert-unix-time.com/api?timestamp=now -K url -F %s -z UTC -c 24 -w 12 -v
    -U        URL to retrieve. (required)
    -K        JSON key to look for date attribute. (required)
    -F        Strftime time format (default: %Y%m%dT%H%M%S). For format details see: man strftime
    -z        Timezone that for the JSON date. Can be "UTC" or UTC offset "-0730"  (default is UTC)
                Can also be "Ameraca/Boston" See: http://search.cpan.org/dist/DateTime-TimeZone/lib/DateTime/TimeZone/Catalog.pm
    -w        Warning if data exceeds this time. (default 12 hours)
    -c        Critical if data exceeds this time. (default 24 hours)
    -u        Time unites. Can be "hours", or "minutes". (default hours)
    -t        Timeout in seconds to wait for the URL to load. (default 60)
    -v        Verbose output.